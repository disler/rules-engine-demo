# rules-engine-demo

Rules engine demo code built to show case general web dev knowledge

I built this project in less than 7 hours so it isn't perfect but the core ideas of front-end code management are there

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
