namespace Types {

	export type RuleEventTriggers = 'Created' | 'Removed' | 'Updated'
	export type RuleConditionOperators = 'is' | 'contains' | 'greater than' | 'less than' | 'equal to'

	export type ANY = 'any'
	export type ALL = 'all'
	export type RuleConditionCombiner = ANY | ALL

	export type RuleEventContext = "Customer" | 'Job' | 'Invoice'
	export type RuleActionNames = 'Create Tag' | 'Notify Customer' | 'Notify Tech'
	export type RuleConditionName = 'Customer Name' | 'Job Number' | 'Customer Revenue' | 'Job Revenue' | 'Customer Age' | 'Invoice Revenue' | 'Referal Source'

	export interface RuleEvent {
		context: RuleEventContext
		trigger: RuleEventTriggers
	}

	export interface RuleConditionBranch {
		any?: Array<RuleCondition | RuleConditionBranch>
		all?: Array<RuleCondition | RuleConditionBranch>
	}

	export interface RuleCondition {
		name: RuleConditionName
		operator: RuleConditionOperators
		value: string | number | null
	}

	export interface RuleAction {
		name: RuleActionNames
		value: string | number | null
	}

	export interface Rule {
		id: string | null
		name: string
		event: RuleEvent
		conditions: RuleConditionBranch
		action: RuleAction
	}

	export interface RootState {
		selectedRule: Rule | null
		rules: Rule[]
	}
}
