import Vue from 'vue'
import Vuex, { ActionContext } from 'vuex'
import {getItem as getLocalStorage, setItem as setLocalStorage} from "@/modules/localStorage"
import {LOCAL_STORAGE_KEY_RULES} from "@/constants"
import * as schemas from "@/schemas";
import { deepCopy, uuid } from "@/modules/utility";
import objectPath from "object-path"
Vue.use(Vuex)

type VuexActionContext = ActionContext<Types.RootState, Types.RootState>

export default new Vuex.Store<Types.RootState>({
  state: {
    selectedRule: null,
    rules: []
  },
  getters: {
    rules: state => state.rules,
    selectedRule: state => state.selectedRule,
  },
  mutations: {
    removeCondition(state, payload) {
      const {path, index} = payload
      if (!state.selectedRule) {
        return
      }
      const conditions = objectPath.get(state.selectedRule.conditions, path)
      conditions.splice(index, 1)
      // with no items left - remove the object
      if (conditions.length === 0) {
        const splitPath = path.split(".")
        if (splitPath.length > 1) {
          const parentPath = splitPath.slice(0, splitPath.length - 2).join(".")
          const parentIndex = splitPath[splitPath.length - 2]
          const parentConditions = objectPath.get(state.selectedRule.conditions, parentPath)
          parentConditions.splice(parentIndex, 1)

        }
      }
    },
    updateConditionField(state, payload) {
      const {value, path} = payload
      if (!state.selectedRule) {
        return
      }
      objectPath.set(state.selectedRule.conditions, path, value)
    },
    updateConditionCombiner(state, payload) {
      const {value, path} = payload
      if (!state.selectedRule) {
        return
      }
      if (path.split(".").length === 1) {
        const conditions = Object.values(state.selectedRule.conditions)[0]
        state.selectedRule.conditions = {[value]: conditions}
      }
      else {
        const conditions = Object.values(objectPath.get(state.selectedRule.conditions, path))[0]
        // not implemented - to save time I'm skipping updated nested combiner values
      }
    },
    cancelRule(state) {
      state.selectedRule = null
    },
    addCondition(state, path: string) {
      if (!state.selectedRule) {
        return
      }
      const conditions = objectPath.get(state.selectedRule.conditions, path)
      const newConditions = [...conditions, schemas.createNewRuleCondition()]
      objectPath.set(state.selectedRule.conditions, path, newConditions)
    },
    addBranch(state, payload) {
      const {combiner, path} = payload
      if (!state.selectedRule) {
        return
      }
      const conditions = objectPath.get(state.selectedRule.conditions, path)
      const newConditions = [...conditions, schemas.createRuleConditionBranchWithCondition(combiner)]
      objectPath.set(state.selectedRule.conditions, path, newConditions)
    },
    archiveRule(state) {
      const id = state.selectedRule?.id
      state.selectedRule = null
      if (!id) {
        return
      }
      state.rules = state.rules.filter(_rule => _rule.id !== id)
      setLocalStorage(LOCAL_STORAGE_KEY_RULES, state.rules)
    },
    updateRuleName(state, value: string) {
      if (!state.selectedRule) {
        return
      }
      state.selectedRule.name = value
    },
    updateRuleActionName(state, value: Types.RuleActionNames) {
      if (!state.selectedRule) {
        return
      }
      state.selectedRule.action.name = value
    },
    updateRuleActionValue(state, value: string | number | null) {
      if (!state.selectedRule) {
        return
      }
      state.selectedRule.action.value = value
    },
    updateRuleEventContext(state, value: Types.RuleEventContext) {
      if (!state.selectedRule) {
        return
      }
      state.selectedRule.event.context = value
    },
    updateRuleEventTrigger(state, value: Types.RuleEventTriggers) {
      if (!state.selectedRule) {
        return
      }
      state.selectedRule.event.trigger = value
    },
    selectRule(state, id: string) {
      state.selectedRule = deepCopy(state.rules.find(_rule => _rule.id === id)) || null
    },
    createNewRule(state) {
      state.selectedRule = schemas.createRule({})
    },
    setRules(state: Types.RootState, rules: Types.Rule[]) {
      state.rules = rules
    },
    saveRule(state: Types.RootState) {
      if (!state.selectedRule) {
        return
      }

      const selectedRule = state.selectedRule

      // create
      if (!selectedRule.id) {
        selectedRule.id = uuid()
        state.rules.push(selectedRule)
      }
      // update
      else {
        state.rules = state.rules.map(_rule => {
          if (_rule.id === selectedRule.id) {
            return selectedRule
          }
          return _rule
        })
      }

      setLocalStorage(LOCAL_STORAGE_KEY_RULES, state.rules)
    },
  },
  actions: {
    /**
     * Mock an api call to fetch json
     */
    async loadRules(context: VuexActionContext) {
      setTimeout(() => {
        const rules = getLocalStorage(LOCAL_STORAGE_KEY_RULES) || []
        context.commit("setRules", rules)
      }, 300)
    }
  },
  modules: {
  }
})
