// https://stackoverflow.com/questions/105034/how-to-create-guid-uuid
export function uuid() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8)
		return v.toString(16)
	})
}

export function deepCopy(object: any) {
	return JSON.parse(JSON.stringify(object))
}
