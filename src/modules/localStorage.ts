/**
 * Minimal local storage module
 */

function isLocalStorageSupported() {
	return typeof window['localStorage'] != "undefined" && window['localStorage'] != null;
}

function createKey(key: string) {
	return `rules-engine-${key}`
}


export function setItem(key: string, value: any) {
	if (!isLocalStorageSupported()) {
		return
	}
	window.localStorage.setItem(createKey(key), JSON.stringify(value));
}

export function getItem(key: string) {
	if (!isLocalStorageSupported()) {
		return
	}
	const value = window.localStorage.getItem(createKey(key))
	if (value) {
		return JSON.parse(value)
	}
	return null
}

export function haveItem(key: string) {
	return Boolean(getItem(key))
}
