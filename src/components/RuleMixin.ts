import { Component, Vue } from 'vue-property-decorator';

@Component
export default class RuleName extends Vue {
  get haveRuleSelected() {
    return Boolean(this.selectedRule)
  }
  get selectedRule(): Types.Rule | null {
    return this.$store.getters.selectedRule
  }
  get conditions() {
      return this.selectedRule?.conditions
  }
}
