/**
 * Generates schemas that align with types.ts
 */

export const createNewRuleEvent = (): Types.RuleEvent => ({
	context: "Customer",
	trigger: "Created",
})

export const createNewRuleCondition = (): Types.RuleCondition => ({
	name: "Customer Name",
	operator: "is",
	value: ''
})

export const createRuleConditionBranchWithCondition = (type: Types.RuleConditionCombiner = "any"): Types.RuleConditionBranch => ({
	[type]: [
		createNewRuleCondition()
	]
})

export const createNewRuleConditionBranch = (type: Types.RuleConditionCombiner = "any"): Types.RuleConditionBranch => ({
	[type]: []
})

export const createNewRuleAction = (): Types.RuleAction => ({
	name: "Create Tag",
	value: null
})

export const createRule = (args: Partial<Types.Rule>): Types.Rule => ({
	name: "",
	id: null,
	event: createNewRuleEvent(),
	conditions: createNewRuleConditionBranch(),
	action: createNewRuleAction(),
	...args,
})
