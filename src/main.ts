import Vue from 'vue'
import App from './App.vue'
import store from './store'

import {LOCAL_STORAGE_KEY_RULES} from "@/constants"
import {haveItem, setItem} from "@/modules/localStorage"
import {createRule} from "@/schemas"
import {uuid} from "@/modules/utility"

// setup a few rules
if (!haveItem(LOCAL_STORAGE_KEY_RULES)) {
  setItem(LOCAL_STORAGE_KEY_RULES, [
    createRule({name: "Rule 1", id: uuid()}),
    createRule({name: "Rule 2", id: uuid()}),
    createRule({name: "Rule 3", id: uuid()}),
  ])
}

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
